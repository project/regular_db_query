<?php

namespace Drupal\regular_db_query\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use \Drupal\Core\Url;

use Drupal\Core\Controller\ControllerBase;

class TaskController extends ControllerBase {

    public function index(){
        $vars = [];

        $list = $this->getTaskListFromDb();
        if(count($list) > 0){
            $vars['content'] = "";
            $vars['list'] = $list;
        }else{
            // draw nothing (you can create new task)
            $vars['content'] = t("Sorry, doesn't find any tasks");
        }


        $rendered = \Drupal::service('theme.manager')->render('tasklist', $vars);

        return [
            '#markup' => $rendered
        ];
    }

    public function getTaskListFromDb(){
        $list = null;

        // sql query
        $connection = \Drupal::database();
        $query = $connection->query("SELECT * FROM regular_db_query_tasks");
        $result = $query->fetchAll();
        // foreach by response
        foreach ($result as $record) {
            $list[] = $record;
            // write each item in $list
        }


        return $list;
    }
    public function delete($task_id){
        $query = \Drupal::database()->delete('regular_db_query_tasks')
            ->condition('id', $task_id)
            ->execute();


        $path = Url::fromRoute('regular_db_query.task_list')->toString();
        $response = new RedirectResponse($path);
        $response->send();
    }


}