<?php

namespace Drupal\regular_db_query\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use \Drupal\Core\Url;

/**
 * Implements an example form.
 */
class CreateForm extends FormBase {

    /**
     * {@inheritdoc}
     */
    public function getFormId() {
        return 'task_create_form';
    }

    protected function createTaskItem($data){
        $db = \Drupal::database();

        $db->insert('regular_db_query_tasks')->fields(
            $data
        )->execute();
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state) {


        $form['name'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Name'),
        );
        $form['time_frequency'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Time frequency'),
            '#description' => $this->t('Only numbers (seconds)'),
        );
        $form['command'] = array(
            '#type' => 'textarea',
            '#title' => $this->t('Command.'),
            '#description' => $this->t('SQL Command. Example <b>TRUNCATE TABLE table_name</b>'),
        );

        $form['actions']['#type'] = 'actions';
        $form['actions']['submit'] = array(
            '#type' => 'submit',
            '#value' => $this->t('Save'),
            '#button_type' => 'primary',
        );
        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state) {

    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {
        $this->createTaskItem([
            "name" => $form_state->getValue('name'),
            "time_frequency" => $form_state->getValue('time_frequency'),
            "command" => $form_state->getValue('command'),
        ]);

        $path = Url::fromRoute('regular_db_query.task_list')->toString();

        $response = new RedirectResponse($path);
        $response->send();
        return;
    }

}