<?php

namespace Drupal\regular_db_query\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use \Drupal\Core\Url;

/**
 * Implements an example form.
 */
class EditForm extends FormBase {

    /**
     * {@inheritdoc}
     */
    public function getFormId() {
        return 'task_edit_form';
    }

    protected function getTaskItemFromDb($task_id){
        $connection = \Drupal::database();
        $query = $connection->query("SELECT * FROM `regular_db_query_tasks` WHERE `id`='{$task_id}'");
        $result = $query->fetchObject();

        return $result;
    }

    protected function updateTaskItem($task_id, $data){
        $db = \Drupal::database();

        $db->update('regular_db_query_tasks')->fields(
            $data
        )
            ->condition('id', $task_id, '=')
            ->execute();
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state, $task_id = null) {
        $data = $this->getTaskItemFromDb($task_id);


        $form['id'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('ID'),
            '#disabled' => TRUE,
            '#default_value' => $data->id
        );
        $form['name'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Name'),
            '#default_value' => $data->name
        );
        $form['time_frequency'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Time frequency'),
            '#description' => $this->t('Only numbers (seconds)'),
            '#default_value' => $data->time_frequency
        );
        $form['command'] = array(
            '#type' => 'textarea',
            '#title' => $this->t('Command'),
            '#default_value' => $data->command,
            '#description' => $this->t('SQL Command. Example <b>TRUNCATE TABLE table_name</b>'),
        );

        $form['actions']['#type'] = 'actions';
        $form['actions']['submit'] = array(
            '#type' => 'submit',
            '#value' => $this->t('Save'),
            '#button_type' => 'primary',
        );
        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state) {

    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {
        $this->updateTaskItem($form_state->getValue('id'), [
            "name" => $form_state->getValue('name'),
            "time_frequency" => $form_state->getValue('time_frequency'),
            "command" => $form_state->getValue('command'),
        ]);

        $path = Url::fromRoute('regular_db_query.task_list')->toString();

        $response = new RedirectResponse($path);
        $response->send();
        return;
    }

}