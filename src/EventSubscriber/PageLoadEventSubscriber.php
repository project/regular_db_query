<?php

/**
 * @file
 * Contains \Drupal\my_event_subscriber\EventSubscriber\MyEventSubscriber.
 */

namespace Drupal\regular_db_query\EventSubscriber;

use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Event Subscriber MyEventSubscriber.
 */
class PageLoadEventSubscriber implements EventSubscriberInterface {

    /**
     * Code that should be triggered on event specified
     */
    public function onRespond(FilterResponseEvent $event) {
        $this->runTaskListCommands();
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents() {
        $events[KernelEvents::RESPONSE][] = ['onRespond'];
        return $events;
    }

    protected function runTaskListCommands(){
        $connection = \Drupal::database();
        $query = $connection->query("SELECT * FROM regular_db_query_tasks");
        $result = $query->fetchAll();
        foreach ($result as $record) {
            $time_different = time() - $record->last_execute_time;
            if($record->last_execute_time == 0 || $time_different > $record->time_frequency){
                try {
                    $connection2 = \Drupal::database();
                    $query = $connection2->query($record->command)->execute();
                } catch (\Exception $e){
                    \Drupal::logger('regular_db_query')->error($e->getMessage());
                }

                $db = \Drupal::database();

                $db->update('regular_db_query_tasks')->fields(
                    ['last_execute_time'=>time()]
                )
                    ->condition('id', $record->id, '=')
                    ->execute();
            }
        }

    }

}